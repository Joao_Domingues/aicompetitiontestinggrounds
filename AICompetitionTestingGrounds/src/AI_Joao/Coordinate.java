
package AI_Joao;

public class Coordinate {
    protected int line;
    protected int column;
    
    protected Coordinate(int i, int j) {
        line = i;
        column = j;
    }
    
    @Override
    public boolean equals(Object o) {
        Coordinate other = (Coordinate) o;
        
        return (other.line == line && other.column == column);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.line;
        hash = 89 * hash + this.column;
        return hash;
    }
    @Override
    public String toString() {
        return "x" + line + " y" + column;
    }
}
