
package AI_Joao_2;

import AI_Joao.*;

public class Coordinate2 {
    protected int line;
    protected int column;
    
    protected Coordinate2(int i, int j) {
        line = i;
        column = j;
    }
    
    @Override
    public boolean equals(Object o) {
        Coordinate2 other = (Coordinate2) o;
        
        return (other.line == line && other.column == column);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.line;
        hash = 89 * hash + this.column;
        return hash;
    }
    @Override
    public String toString() {
        return "x" + line + " y" + column;
    }
}
