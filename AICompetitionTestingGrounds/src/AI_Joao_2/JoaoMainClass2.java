package AI_Joao_2;

import Utils.GraphAlgorithms;
import Utils.Graph;
import aicompetitiontestinggrounds.Avatar;
import aicompetitiontestinggrounds.GameMechanism;
import aicompetitiontestinggrounds.Orientation;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class JoaoMainClass2 {

    private int myCurrentX;
    private int myCurrentY;

    private int maxLines;
    private int maxColumns;

    private final int DEFENSIVE_TURN = 0;
    private final int OFFENSIVE_TURN = 1;
    private final int REPOSITIONING_TURN = 2;

    private final int MOVE_NORTH = 1;
    private final int MOVE_SOUTH = 2;
    private final int MOVE_EAST = 3;
    private final int MOVE_WEST = 4;

    private final int SHOT_NORTH = 5;
    private final int SHOT_SOUTH = 6;
    private final int SHOT_EAST = 7;
    private final int SHOT_WEST = 8;

    private boolean firstTurn = true;
    List<Integer> strategyID = new ArrayList<>();

    Graph<Coordinate2, Integer> graph = new Graph(false);
    Graph<Coordinate2, Integer> graphClone = new Graph(false);
    List<Integer> actionID = new ArrayList<>();
    List<Integer> highPriorityActionQueue = new ArrayList<>();
    List<Integer> lowPriorityActionQueue = new ArrayList<>();

    public JoaoMainClass2() {

    }

    // <editor-fold defaultstate="collapsed" desc="Example">  
    // </editor-fold> 
    public void joaoMove(Avatar avatar, GameMechanism gameMechanism) {

        myCurrentX = avatar.xCoordinate();
        myCurrentY = avatar.yCoordinate();

        maxLines = gameMechanism.boardNumberOfLines();
        maxColumns = gameMechanism.boardNumberOfColumns();

        Coordinate2 coordinate;
        Coordinate2 coordinate2;

        LinkedList<Coordinate2> shortPath = new LinkedList<>();

        // <editor-fold defaultstate="collapsed" desc="Graph Generator (First Turn Only)">  
        if (firstTurn) {
            for (int i = 0; i < maxLines; i++) {
                for (int j = 0; j < maxColumns; j++) {
                    if (!gameMechanism.squareHasObstacle(i, j)) {
                        coordinate = new Coordinate2(i, j);
                        graph.insertVertex(coordinate);
                    }
                }
            }

            for (int i = 0; i < maxLines; i++) {
                for (int j = 0; j < maxColumns; j++) {
                    if (!gameMechanism.squareHasObstacle(i, j)) {
                        if (validatePosition(i + 1, j)) {
                            if (!gameMechanism.squareHasObstacle(i + 1, j)) {
                                coordinate = new Coordinate2(i, j);
                                coordinate2 = new Coordinate2(i + 1, j);
                                graph.insertEdge(coordinate, coordinate2, 1, 1);
                            }
                        }
                        if (validatePosition(i - 1, j)) {
                            if (!gameMechanism.squareHasObstacle(i - 1, j)) {
                                coordinate = new Coordinate2(i, j);
                                coordinate2 = new Coordinate2(i - 1, j);
                                graph.insertEdge(coordinate, coordinate2, 1, 1);
                            }
                        }
                        if (validatePosition(i, j + 1)) {
                            if (!gameMechanism.squareHasObstacle(i, j + 1)) {
                                coordinate = new Coordinate2(i, j);
                                coordinate2 = new Coordinate2(i, j + 1);
                                graph.insertEdge(coordinate, coordinate2, 1, 1);
                            }
                        }
                        if (validatePosition(i, j - 1)) {
                            if (!gameMechanism.squareHasObstacle(i, j - 1)) {
                                coordinate = new Coordinate2(i, j);
                                coordinate2 = new Coordinate2(i, j - 1);
                                graph.insertEdge(coordinate, coordinate2, 1, 1);
                            }
                        }
                    }
                }
            }
            firstTurn = false;
        }
        // </editor-fold> 

        System.out.println("\nPlayer ID: 2");
        System.out.println("Current position - x: " + myCurrentX + " y: " + myCurrentY);
        
        // <editor-fold defaultstate="collapsed" desc="High Priority Queue">  
        if (!highPriorityActionQueue.isEmpty()) {
            switch (highPriorityActionQueue.get(0)) {
                case MOVE_NORTH:
                    gameMechanism.playerMoveAction(avatar, Orientation.options.N);
                    strategyID.add(REPOSITIONING_TURN);
                    actionID.add(MOVE_NORTH);
                    highPriorityActionQueue.clear();
                    return;
                case MOVE_SOUTH:
                    gameMechanism.playerMoveAction(avatar, Orientation.options.S);
                    strategyID.add(REPOSITIONING_TURN);
                    actionID.add(MOVE_SOUTH);
                    highPriorityActionQueue.clear();
                    return;
                case MOVE_EAST:
                    gameMechanism.playerMoveAction(avatar, Orientation.options.E);
                    strategyID.add(REPOSITIONING_TURN);
                    actionID.add(MOVE_EAST);
                    highPriorityActionQueue.clear();
                    return;
                case MOVE_WEST:
                    gameMechanism.playerMoveAction(avatar, Orientation.options.W);
                    strategyID.add(REPOSITIONING_TURN);
                    actionID.add(MOVE_WEST);
                    highPriorityActionQueue.clear();
                    return;
                default:
                    break;
            }
        }
        // </editor-fold> 

        // <editor-fold defaultstate="collapsed" desc="Block Enemy Active Shot"> 
        if (blockAdjacentEnemyActiveShot(avatar, gameMechanism)) {
            strategyID.add(DEFENSIVE_TURN);
            return;
        }
        // </editor-fold>

        if ((double) countMySquares(gameMechanism)/countOpponentSquares(gameMechanism) > 1.1) {
            // <editor-fold defaultstate="collapsed" desc="Conquer 1-Square Dead Ends"> 
            if (corridorIsA1SquareDeadEnd(avatar, gameMechanism, Orientation.options.N)) {
                if (gameMechanism.playerMoveAction(avatar, Orientation.options.N)) {
                    strategyID.add(REPOSITIONING_TURN);
                    highPriorityActionQueue.add(MOVE_SOUTH);
                    return;
                }
            }
            if (corridorIsA1SquareDeadEnd(avatar, gameMechanism, Orientation.options.S)) {
                if (gameMechanism.playerMoveAction(avatar, Orientation.options.S)) {
                    strategyID.add(REPOSITIONING_TURN);
                    highPriorityActionQueue.add(MOVE_NORTH);
                    return;
                }
            }
            if (corridorIsA1SquareDeadEnd(avatar, gameMechanism, Orientation.options.W)) {
                if (gameMechanism.playerMoveAction(avatar, Orientation.options.W)) {
                    strategyID.add(REPOSITIONING_TURN);
                    highPriorityActionQueue.add(MOVE_EAST);
                    return;
                }
            }
            if (corridorIsA1SquareDeadEnd(avatar, gameMechanism, Orientation.options.E)) {
                if (gameMechanism.playerMoveAction(avatar, Orientation.options.E)) {
                    strategyID.add(REPOSITIONING_TURN);
                    highPriorityActionQueue.add(MOVE_WEST);
                    return;
                }
            }
            // </editor-fold> 
        }

        // <editor-fold defaultstate="collapsed" desc="Shoot in place to the best direction">  
        int maxValue = 0, result = 0;
        if (countUnconqueredSquareToNorthOf(myCurrentX, myCurrentY, gameMechanism) > maxValue) {
            maxValue = countUnconqueredSquareToNorthOf(myCurrentX, myCurrentY, gameMechanism);
            result = 1;
        }
        if (countUnconqueredSquareToSouthOf(myCurrentX, myCurrentY, gameMechanism) > maxValue) {
            maxValue = countUnconqueredSquareToSouthOf(myCurrentX, myCurrentY, gameMechanism);
            result = 2;
        }
        if (countUnconqueredSquareToEastOf(myCurrentX, myCurrentY, gameMechanism) > maxValue) {
            maxValue = countUnconqueredSquareToEastOf(myCurrentX, myCurrentY, gameMechanism);
            result = 3;
        }
        if (countUnconqueredSquareToWestOf(myCurrentX, myCurrentY, gameMechanism) > maxValue) {
            result = 4;
        }
        if (maxValue > 1) {
            switch (result) {
                case 1:
                    if (!scanForShotInADirection(avatar, Orientation.options.N, gameMechanism)) {
                        if (!lowPriorityActionQueue.isEmpty()) {
                            if (lowPriorityActionQueue.get(0) != MOVE_NORTH) {
                                if (!actionID.isEmpty()) {
                                    if (actionID.get(actionID.size() - 1) != SHOT_NORTH) {
                                        if (gameMechanism.playerShootAction(avatar, Orientation.options.N)) {
                                            strategyID.add(OFFENSIVE_TURN);
                                            actionID.add(SHOT_NORTH);
                                            return;
                                        }
                                    }
                                } else {
                                    if (gameMechanism.playerShootAction(avatar, Orientation.options.N)) {
                                        strategyID.add(OFFENSIVE_TURN);
                                        actionID.add(SHOT_NORTH);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    break;
                case 2:
                    if (!scanForShotInADirection(avatar, Orientation.options.S, gameMechanism)) {
                        if (!lowPriorityActionQueue.isEmpty()) {
                            if (lowPriorityActionQueue.get(0) != MOVE_SOUTH) {
                                if (!actionID.isEmpty()) {
                                    if (actionID.get(actionID.size() - 1) != SHOT_SOUTH) {
                                        if (gameMechanism.playerShootAction(avatar, Orientation.options.S)) {
                                            strategyID.add(OFFENSIVE_TURN);
                                            actionID.add(SHOT_SOUTH);
                                            return;
                                        }
                                    }
                                } else {
                                    if (gameMechanism.playerShootAction(avatar, Orientation.options.S)) {
                                        strategyID.add(OFFENSIVE_TURN);
                                        actionID.add(SHOT_SOUTH);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    break;
                case 3:
                    if (!scanForShotInADirection(avatar, Orientation.options.E, gameMechanism)) {
                        if (!lowPriorityActionQueue.isEmpty()) {
                            if (lowPriorityActionQueue.get(0) != MOVE_EAST) {
                                if (!actionID.isEmpty()) {
                                    if (actionID.get(actionID.size() - 1) != SHOT_EAST) {
                                        if (gameMechanism.playerShootAction(avatar, Orientation.options.E)) {
                                            strategyID.add(OFFENSIVE_TURN);
                                            actionID.add(SHOT_EAST);
                                            return;
                                        }
                                    }
                                } else {
                                    if (gameMechanism.playerShootAction(avatar, Orientation.options.E)) {
                                        strategyID.add(OFFENSIVE_TURN);
                                        actionID.add(SHOT_EAST);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    break;
                case 4:
                    if (!scanForShotInADirection(avatar, Orientation.options.W, gameMechanism)) {
                        if (!lowPriorityActionQueue.isEmpty()) {
                            if (lowPriorityActionQueue.get(0) == MOVE_WEST) {
                                if (!actionID.isEmpty()) {
                                    if (actionID.get(actionID.size() - 1) != SHOT_WEST) {
                                        if (gameMechanism.playerShootAction(avatar, Orientation.options.W)) {
                                            strategyID.add(OFFENSIVE_TURN);
                                            actionID.add(SHOT_WEST);
                                            return;
                                        }
                                    }
                                } else {
                                    if (gameMechanism.playerShootAction(avatar, Orientation.options.W)) {
                                        strategyID.add(OFFENSIVE_TURN);
                                        actionID.add(SHOT_WEST);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Decide where to go">  
        coordinate2 = new Coordinate2(0, 0);
        coordinate = new Coordinate2(myCurrentX, myCurrentY);

        for (int i = 0; i < maxLines; i++) {
            for (int j = 0; j < maxColumns; j++) {
                if (gameMechanism.squareHasPlayer(i, j) && (i != myCurrentX || j != myCurrentY)) {
                    coordinate2.line = i;
                    coordinate2.column = j;
                    break;
                }
            }
        }
        
        if (countMySquares(gameMechanism) <= countOpponentSquares(gameMechanism) && lowPriorityActionQueue.isEmpty()) {
            int quadrant = 0, totalSquareCount, mySquares;
            boolean hasPlayer = false;
            double percentage = 1;

            totalSquareCount = 0;
            mySquares = 0;
            for (int i = 0; i < maxLines / 2; i++) {
                for (int j = 0; j < maxColumns / 2; j++) {
                    if (!gameMechanism.squareHasObstacle(i, j)) {
                        totalSquareCount++;
                        if (gameMechanism.squareBelongsToPlayer2(i, j)) {
                            mySquares++;
                        }
                        if (gameMechanism.squareHasPlayer(i, j) && (i != myCurrentX || j != myCurrentY)) {
                            hasPlayer = true;
                        }
                    }
                }
            }
            if (!hasPlayer) {
                if (percentage >= (double) mySquares / totalSquareCount) {
                    quadrant = 1;
                    percentage = (double) mySquares / totalSquareCount;
                }
            }
            System.out.println("%NW: " + (double) mySquares / totalSquareCount);
            hasPlayer = false;
            totalSquareCount = 0;
            mySquares = 0;
            for (int i = maxLines / 2; i < maxLines; i++) {
                for (int j = 0; j < maxColumns / 2; j++) {
                    if (!gameMechanism.squareHasObstacle(i, j)) {
                        totalSquareCount++;
                        if (gameMechanism.squareBelongsToPlayer2(i, j)) {
                            mySquares++;
                        }
                        if (gameMechanism.squareHasPlayer(i, j) && (i != myCurrentX || j != myCurrentY)) {
                            hasPlayer = true;
                        }
                    }
                }
            }
            if (!hasPlayer) {
                if (percentage >= (double) mySquares / totalSquareCount) {
                    quadrant = 2;
                    percentage = (double) mySquares / totalSquareCount;
                }
            }
            System.out.println("%NE: " + (double) mySquares / totalSquareCount);
            hasPlayer = false;
            totalSquareCount = 0;
            mySquares = 0;
            for (int i = 0; i < maxLines / 2; i++) {
                for (int j = maxColumns / 2; j < maxColumns; j++) {
                    if (!gameMechanism.squareHasObstacle(i, j)) {
                        totalSquareCount++;
                        if (gameMechanism.squareBelongsToPlayer2(i, j)) {
                            mySquares++;
                        }
                        if (gameMechanism.squareHasPlayer(i, j) && (i != myCurrentX || j != myCurrentY)) {
                            hasPlayer = true;
                        }
                    }
                }
            }
            if (!hasPlayer) {
                if (percentage >= (double) mySquares / totalSquareCount) {
                    quadrant = 3;
                    percentage = (double) mySquares / totalSquareCount;
                }
            }
            System.out.println("%SW: " + (double) mySquares / totalSquareCount);
            hasPlayer = false;
            totalSquareCount = 0;
            mySquares = 0;
            for (int i = maxLines / 2; i < maxLines; i++) {
                for (int j = maxColumns / 2; j < maxColumns; j++) {
                    if (!gameMechanism.squareHasObstacle(i, j)) {
                        totalSquareCount++;
                        if (gameMechanism.squareBelongsToPlayer2(i, j)) {
                            mySquares++;
                        }
                        if (gameMechanism.squareHasPlayer(i, j) && (i != myCurrentX || j != myCurrentY)) {
                            hasPlayer = true;
                        }
                    }
                }
            }
            if (!hasPlayer) {
                if (percentage >= (double) mySquares / totalSquareCount) {
                    quadrant = 4;
                    percentage = (double) mySquares / totalSquareCount;
                }
            }
            System.out.println("%SE: " + (double) mySquares / totalSquareCount);
            System.out.println("Quadrant: " + quadrant);

            int i, j;
            switch (quadrant) {
                case 1:
                    do{
                        i = (int) (Math.random() * (maxLines/2 - 0)) + 0;
                        j = (int) (Math.random() * (maxColumns/2 - 0)) + 0;
                    }while(gameMechanism.squareHasObstacle(i, j) || (i == myCurrentX && j == myCurrentY));
                    coordinate2.line = i;
                    coordinate2.column = j;
//                    for (int i = 0; i < maxLines / 2; i++) {
//                        for (int j = 0; j < maxColumns / 2; j++) {
//                            if (!gameMechanism.squareHasObstacle(i, j) && (i != myCurrentX || j != myCurrentY) && !gameMechanism.squareBelongsToPlayer1(i, j)) {
//                                coordinate2.line = i;
//                                coordinate2.column = j;
//                                break;
//                            }
//                        }
//                    }
                    break;
                case 2:
                    do{
                        i = (int) (Math.random() * (maxLines - maxLines/2)) + maxLines/2;
                        j = (int) (Math.random() * (maxColumns/2 - 0)) + 0;
                    }while(gameMechanism.squareHasObstacle(i, j) || (i == myCurrentX && j == myCurrentY));
                    coordinate2.line = i;
                    coordinate2.column = j;
//                    for (int i = maxLines / 2; i < maxLines; i++) {
//                        for (int j = 0; j < maxColumns / 2; j++) {
//                            if (!gameMechanism.squareHasObstacle(i, j) && (i != myCurrentX || j != myCurrentY) && !gameMechanism.squareBelongsToPlayer1(i, j)) {
//                                coordinate2.line = i;
//                                coordinate2.column = j;
//                                break;
//                            }
//                        }
//                    }
                    break;
                case 3:
                    do{
                        i = (int) (Math.random() * (maxLines/2 - 0)) + 0;
                        j = (int) (Math.random() * (maxLines/2 - maxColumns/2)) + maxColumns/2;
                    }while(gameMechanism.squareHasObstacle(i, j) || (i == myCurrentX && j == myCurrentY));
                    coordinate2.line = i;
                    coordinate2.column = j;
//                    for (int i = 0; i < maxLines / 2; i++) {
//                        for (int j = maxColumns / 2; j < maxColumns; j++) {
//                            if (!gameMechanism.squareHasObstacle(i, j) && (i != myCurrentX || j != myCurrentY) && !gameMechanism.squareBelongsToPlayer1(i, j)) {
//                                coordinate2.line = i;
//                                coordinate2.column = j;
//                                break;
//                            }
//                        }
//                    }
                    break;
                case 4:
                    do{
                        i = (int) (Math.random() * (maxLines - maxLines/2)) + maxLines/2;
                        j = (int) (Math.random() * (maxColumns - maxColumns/2)) + maxColumns/2;
                    }while(gameMechanism.squareHasObstacle(i, j) || (i == myCurrentX && j == myCurrentY));
                    coordinate2.line = i;
                    coordinate2.column = j;
//                    for (int i = maxLines / 2; i < maxLines; i++) {
//                        for (int j = maxColumns / 2; j < maxColumns; j++) {
//                            if (!gameMechanism.squareHasObstacle(i, j) && (i != myCurrentX || j != myCurrentY) && !gameMechanism.squareBelongsToPlayer1(i, j)) {
//                                coordinate2.line = i;
//                                coordinate2.column = j;
//                                break;
//                            }
//                        }
//                    }
                    break;
                default:
                    break;
            }

        }

        // </editor-fold> 
        
        System.out.println("Heading to x: " + coordinate2.line + " y: " + coordinate2.column);

        // <editor-fold defaultstate="collapsed" desc="Shortest Path">  
        if (countMySquares(gameMechanism) < countOpponentSquares(gameMechanism)) {
            lowPriorityActionQueue.clear();
        }
        GraphAlgorithms.shortestPath(graph, coordinate, coordinate2, shortPath);

        Iterator it = shortPath.iterator();
        Iterator it2 = shortPath.iterator();

        if (it2.hasNext()) {
            it2.next();
            while (it2.hasNext()) {
                coordinate = (Coordinate2) it.next();
                coordinate2 = (Coordinate2) it2.next();

                if (coordinate.line > coordinate2.line) {
                    lowPriorityActionQueue.add(MOVE_WEST);
                }
                if (coordinate.line < coordinate2.line) {
                    lowPriorityActionQueue.add(MOVE_EAST);
                }
                if (coordinate.column > coordinate2.column) {
                    lowPriorityActionQueue.add(MOVE_NORTH);
                }
                if (coordinate.column < coordinate2.column) {
                    lowPriorityActionQueue.add(MOVE_SOUTH);
                }
            }
        }
        // </editor-fold> 

        System.out.println("LPQ: " + lowPriorityActionQueue.toString());

        // <editor-fold defaultstate="collapsed" desc="Low Priority Queue">  
        if (!lowPriorityActionQueue.isEmpty()) {
            switch (lowPriorityActionQueue.get(0)) {
                case MOVE_NORTH:
                    gameMechanism.playerMoveAction(avatar, Orientation.options.N);
                    strategyID.add(REPOSITIONING_TURN);
                    actionID.add(MOVE_NORTH);
                    lowPriorityActionQueue.remove(0);
                    return;
                case MOVE_SOUTH:
                    gameMechanism.playerMoveAction(avatar, Orientation.options.S);
                    strategyID.add(REPOSITIONING_TURN);
                    actionID.add(MOVE_SOUTH);
                    lowPriorityActionQueue.remove(0);
                    return;
                case MOVE_EAST:
                    gameMechanism.playerMoveAction(avatar, Orientation.options.E);
                    strategyID.add(REPOSITIONING_TURN);
                    actionID.add(MOVE_EAST);
                    lowPriorityActionQueue.remove(0);
                    return;
                case MOVE_WEST:
                    gameMechanism.playerMoveAction(avatar, Orientation.options.W);
                    strategyID.add(REPOSITIONING_TURN);
                    actionID.add(MOVE_WEST);
                    lowPriorityActionQueue.remove(0);
                    return;
                default:
                    break;
            }
        }
        // </editor-fold>

        System.out.println("NO ACTION DONE");
    }

    // <editor-fold defaultstate="collapsed" desc="Block Enemy Shot Function">  
    /**
     * Tries to block enemy shots that only require 1 move to block
     *
     * @param avatar
     * @param gameMechanism
     * @return
     */
    private boolean blockAdjacentEnemyActiveShot(Avatar avatar, GameMechanism gameMechanism) {
        int i;
        int j;
        Orientation.options orientation;

        // N
        i = myCurrentX;
        j = myCurrentY - 2;

        if (validatePosition(i, j)) {
            if (gameMechanism.squareHasActiveShotFromPlayer1(i, j)) {
                orientation = gameMechanism.orientationOfActiveShot(i, j);
                if (orientation == Orientation.options.S) {
                    if (gameMechanism.playerShootAction(avatar, Orientation.options.N)) {
                        actionID.add(SHOT_NORTH);
                        highPriorityActionQueue.add(SHOT_NORTH);
                        return true;
                    }
                }
            }
        }

        // S
        i = myCurrentX;
        j = myCurrentY + 2;

        if (validatePosition(i, j)) {
            if (gameMechanism.squareHasActiveShotFromPlayer1(i, j)) {
                orientation = gameMechanism.orientationOfActiveShot(i, j);
                if (orientation == Orientation.options.N) {
                    if (gameMechanism.playerShootAction(avatar, Orientation.options.S)) {
                        actionID.add(SHOT_SOUTH);
                        highPriorityActionQueue.add(SHOT_SOUTH);
                        return true;
                    }
                }
            }
        }

        // E
        i = myCurrentX + 2;
        j = myCurrentY;

        if (validatePosition(i, j)) {
            if (gameMechanism.squareHasActiveShotFromPlayer1(i, j)) {
                orientation = gameMechanism.orientationOfActiveShot(i, j);
                if (orientation == Orientation.options.W) {
                    if (gameMechanism.playerShootAction(avatar, Orientation.options.E)) {
                        actionID.add(SHOT_EAST);
                        highPriorityActionQueue.add(SHOT_EAST);
                        return true;
                    }
                }
            }
        }

        // W
        i = myCurrentX;
        j = myCurrentY - 2;

        if (validatePosition(i, j)) {
            if (gameMechanism.squareHasActiveShotFromPlayer1(i, j)) {
                orientation = gameMechanism.orientationOfActiveShot(i, j);
                if (orientation == Orientation.options.E) {
                    if (gameMechanism.playerShootAction(avatar, Orientation.options.W)) {
                        actionID.add(SHOT_WEST);
                        highPriorityActionQueue.add(SHOT_WEST);
                        return true;
                    }
                }
            }
        }

        // SE
        i = myCurrentX + 1;
        j = myCurrentY + 1;

        if (validatePosition(i, j)) {
            if (gameMechanism.squareHasActiveShotFromPlayer1(i, j)) {
                orientation = gameMechanism.orientationOfActiveShot(i, j);
                if (orientation == Orientation.options.N) {
                    if (gameMechanism.playerShootAction(avatar, Orientation.options.E)) {
                        actionID.add(SHOT_EAST);
                        highPriorityActionQueue.add(SHOT_EAST);
                        return true;
                    }
                    if (gameMechanism.playerMoveAction(avatar, Orientation.options.E)) {
                        actionID.add(MOVE_EAST);
                        return true;
                    }
                }
                if (orientation == Orientation.options.W) {
                    if (gameMechanism.playerShootAction(avatar, Orientation.options.S)) {
                        actionID.add(SHOT_SOUTH);
                        highPriorityActionQueue.add(SHOT_SOUTH);
                        return true;
                    }
                    if (gameMechanism.playerMoveAction(avatar, Orientation.options.S)) {
                        actionID.add(MOVE_SOUTH);
                        return true;
                    }
                }
            }
        }

        // NW
        i = myCurrentX - 1;
        j = myCurrentY - 1;

        if (validatePosition(i, j)) {
            if (gameMechanism.squareHasActiveShotFromPlayer1(i, j)) {
                orientation = gameMechanism.orientationOfActiveShot(i, j);
                if (orientation == Orientation.options.S) {
                    if (gameMechanism.playerShootAction(avatar, Orientation.options.W)) {
                        actionID.add(SHOT_WEST);
                        highPriorityActionQueue.add(SHOT_WEST);
                        return true;
                    }
                    if (gameMechanism.playerMoveAction(avatar, Orientation.options.W)) {
                        actionID.add(MOVE_WEST);
                        return true;
                    }
                }
                if (orientation == Orientation.options.E) {
                    if (gameMechanism.playerShootAction(avatar, Orientation.options.N)) {
                        actionID.add(SHOT_NORTH);
                        highPriorityActionQueue.add(SHOT_NORTH);
                        return true;
                    }
                    if (gameMechanism.playerMoveAction(avatar, Orientation.options.N)) {
                        actionID.add(MOVE_NORTH);
                        return true;
                    }
                }
            }
        }

        // SW
        i = myCurrentX - 1;
        j = myCurrentY + 1;

        if (validatePosition(i, j)) {
            if (gameMechanism.squareHasActiveShotFromPlayer1(i, j)) {
                orientation = gameMechanism.orientationOfActiveShot(i, j);
                if (orientation == Orientation.options.N) {
                    if (gameMechanism.playerShootAction(avatar, Orientation.options.W)) {
                        actionID.add(SHOT_WEST);
                        highPriorityActionQueue.add(SHOT_WEST);
                        return true;
                    }
                    if (gameMechanism.playerMoveAction(avatar, Orientation.options.W)) {
                        actionID.add(MOVE_WEST);
                        return true;
                    }
                }
                if (orientation == Orientation.options.E) {
                    if (gameMechanism.playerShootAction(avatar, Orientation.options.S)) {
                        actionID.add(SHOT_SOUTH);
                        highPriorityActionQueue.add(SHOT_SOUTH);
                        return true;
                    }
                    if (gameMechanism.playerMoveAction(avatar, Orientation.options.S)) {
                        actionID.add(MOVE_SOUTH);
                        return true;
                    }
                }
            }
        }

        // NE
        i = myCurrentX + 1;
        j = myCurrentY - 1;

        if (validatePosition(i, j)) {
            if (gameMechanism.squareHasActiveShotFromPlayer1(i, j)) {
                orientation = gameMechanism.orientationOfActiveShot(i, j);
                if (orientation == Orientation.options.S) {
                    if (gameMechanism.playerShootAction(avatar, Orientation.options.E)) {
                        actionID.add(SHOT_EAST);
                        highPriorityActionQueue.add(SHOT_EAST);
                        return true;
                    }
                    if (gameMechanism.playerMoveAction(avatar, Orientation.options.E)) {
                        actionID.add(MOVE_EAST);
                        return true;
                    }
                }
                if (orientation == Orientation.options.W) {
                    if (gameMechanism.playerShootAction(avatar, Orientation.options.N)) {
                        actionID.add(SHOT_NORTH);
                        highPriorityActionQueue.add(SHOT_NORTH);
                        return true;
                    }
                    if (gameMechanism.playerMoveAction(avatar, Orientation.options.N)) {
                        actionID.add(MOVE_NORTH);
                        return true;
                    }
                }
            }
        }

        // long SE
        i = myCurrentX;
        j = myCurrentY;

        int x, y;
        boolean valid = true;

        while (validatePosition(i, j) && valid) {
            if (gameMechanism.squareHasActiveShotFromPlayer1(i, j)) {
                orientation = gameMechanism.orientationOfActiveShot(i, j);
                if (orientation == Orientation.options.N) {
                    for (x = myCurrentX; x <= i; x++) {
                        if (gameMechanism.squareHasObstacle(x, j)) {
                            valid = false;
                        }
                    }
                    for (y = myCurrentY; y <= j; y++) {
                        if (gameMechanism.squareHasObstacle(i, y)) {
                            valid = false;
                        }
                    }
                    if (valid) {
                        if (gameMechanism.playerShootAction(avatar, Orientation.options.E)) {
                            actionID.add(SHOT_EAST);
                            return true;
                        }
                    }
                }
                if (orientation == Orientation.options.W) {
                    for (y = myCurrentY; y <= j; y++) {
                        if (gameMechanism.squareHasObstacle(i, y)) {
                            valid = false;
                        }
                    }
                    for (x = myCurrentX; x <= i; x++) {
                        if (gameMechanism.squareHasObstacle(x, j)) {
                            valid = false;
                        }
                    }
                    if (valid) {
                        if (gameMechanism.playerShootAction(avatar, Orientation.options.S)) {
                            actionID.add(SHOT_SOUTH);
                            return true;
                        }
                    }
                }
            }
            i++;
            j++;
        }

        // long NE
        i = myCurrentX;
        j = myCurrentY;

        valid = true;

        while (validatePosition(i, j) && valid) {
            if (gameMechanism.squareHasActiveShotFromPlayer1(i, j)) {
                orientation = gameMechanism.orientationOfActiveShot(i, j);
                if (orientation == Orientation.options.W) {
                    for (x = myCurrentX; x >= i; x--) {
                        if (gameMechanism.squareHasObstacle(x, j)) {
                            valid = false;
                        }
                    }
                    for (y = myCurrentY; y <= j; y++) {
                        if (gameMechanism.squareHasObstacle(i, y)) {
                            valid = false;
                        }
                    }
                    if (valid) {
                        if (gameMechanism.playerShootAction(avatar, Orientation.options.N)) {
                            actionID.add(SHOT_NORTH);
                            return true;
                        }
                    }
                }
                if (orientation == Orientation.options.S) {
                    for (x = myCurrentX; x <= i; x++) {
                        if (gameMechanism.squareHasObstacle(x, j)) {
                            valid = false;
                        }
                    }
                    for (y = myCurrentY; y <= j; y--) {
                        if (gameMechanism.squareHasObstacle(i, y)) {
                            valid = false;
                        }
                    }
                    if (valid) {
                        if (gameMechanism.playerShootAction(avatar, Orientation.options.E)) {
                            actionID.add(SHOT_EAST);
                            return true;
                        }
                    }
                }
            }
            i++;
            j--;
        }

        // long SW
        i = myCurrentX;
        j = myCurrentY;

        valid = true;

        while (validatePosition(i, j) && valid) {
            if (gameMechanism.squareHasActiveShotFromPlayer1(i, j)) {
                orientation = gameMechanism.orientationOfActiveShot(i, j);
                if (orientation == Orientation.options.E) {
                    for (y = myCurrentY; y <= j; y++) {
                        if (gameMechanism.squareHasObstacle(i, y)) {
                            valid = false;
                        }
                    }
                    for (x = myCurrentX; x >= i; x--) {
                        if (gameMechanism.squareHasObstacle(x, j)) {
                            valid = false;
                        }
                    }
                    if (valid) {
                        if (gameMechanism.playerShootAction(avatar, Orientation.options.S)) {
                            actionID.add(SHOT_SOUTH);
                            return true;
                        }
                    }
                }
                if (orientation == Orientation.options.N) {
                    for (x = myCurrentX; x <= i; x--) {
                        if (gameMechanism.squareHasObstacle(x, j)) {
                            valid = false;
                        }
                    }
                    for (y = myCurrentY; y <= j; y++) {
                        if (gameMechanism.squareHasObstacle(i, y)) {
                            valid = false;
                        }
                    }
                    if (valid) {
                        if (gameMechanism.playerShootAction(avatar, Orientation.options.W)) {
                            actionID.add(SHOT_WEST);
                            return true;
                        }
                    }
                }
            }
            i--;
            j++;
        }

        // long NW
        i = myCurrentX;
        j = myCurrentY;

        valid = true;

        while (validatePosition(i, j) && valid) {
            if (gameMechanism.squareHasActiveShotFromPlayer1(i, j)) {
                orientation = gameMechanism.orientationOfActiveShot(i, j);
                if (orientation == Orientation.options.E) {
                    for (y = myCurrentY; y <= j; y--) {
                        if (gameMechanism.squareHasObstacle(i, y)) {
                            valid = false;
                        }
                    }
                    for (x = myCurrentX; x >= i; x--) {
                        if (gameMechanism.squareHasObstacle(x, j)) {
                            valid = false;
                        }
                    }
                    if (valid) {
                        if (gameMechanism.playerShootAction(avatar, Orientation.options.N)) {
                            actionID.add(SHOT_NORTH);
                            return true;
                        }
                    }
                }
                if (orientation == Orientation.options.S) {
                    for (y = myCurrentY; y <= j; y--) {
                        if (gameMechanism.squareHasObstacle(i, y)) {
                            valid = false;
                        }
                    }
                    for (x = myCurrentX; x <= i; x--) {
                        if (gameMechanism.squareHasObstacle(x, j)) {
                            valid = false;
                        }
                    }
                    if (valid) {
                        if (gameMechanism.playerShootAction(avatar, Orientation.options.W)) {
                            actionID.add(SHOT_WEST);
                            return true;
                        }
                    }
                }
            }
            i--;
            j--;
        }
        return false;
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Counter of Unconquered Squares In a Straight Line From XY Position">  
    public int countUnconqueredSquareToEastOf(int i, int j, GameMechanism gameMechanism) {
        int total = 0;
        i++;
        while (validatePosition(i, j) && !gameMechanism.squareHasObstacle(i, j)) {
            if (!gameMechanism.squareBelongsToPlayer2(i, j)) {
                total++;
            }
            i++;
        }
        return total;
    }

    public int countUnconqueredSquareToWestOf(int i, int j, GameMechanism gameMechanism) {
        int total = 0;
        i--;
        while (validatePosition(i, j) && !gameMechanism.squareHasObstacle(i, j)) {
            if (!gameMechanism.squareBelongsToPlayer2(i, j)) {
                total++;
            }
            i--;
        }
        return total;
    }

    public int countUnconqueredSquareToNorthOf(int i, int j, GameMechanism gameMechanism) {
        int total = 0;
        j--;
        while (validatePosition(i, j) && !gameMechanism.squareHasObstacle(i, j)) {
            if (!gameMechanism.squareBelongsToPlayer2(i, j)) {
                total++;
            }
            j--;
        }
        return total;
    }

    public int countUnconqueredSquareToSouthOf(int i, int j, GameMechanism gameMechanism) {
        int total = 0;
        j++;
        while (validatePosition(i, j) && !gameMechanism.squareHasObstacle(i, j)) {
            if (!gameMechanism.squareBelongsToPlayer2(i, j)) {
                total++;
            }
            j++;
        }
        return total;
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Team Square Counter">  
    private int countOpponentSquares(GameMechanism gameMechanism) {
        int total = 0;

        for (int i = 0; i < maxLines; i++) {
            for (int j = 0; j < maxColumns; j++) {
                if (gameMechanism.squareBelongsToPlayer1(i, j)) {
                    total++;
                }
            }
        }

        return total;
    }

    private int countMySquares(GameMechanism gameMechanism) {
        int total = 0;

        for (int i = 0; i < maxLines; i++) {
            for (int j = 0; j < maxColumns; j++) {
                if (gameMechanism.squareBelongsToPlayer2(i, j)) {
                    total++;
                }
            }
        }

        return total;
    }
    // </editor-fold> 

    private boolean corridorIsA1SquareDeadEnd(Avatar avatar, GameMechanism gameMechanism, Orientation.options orientation) {
        switch (orientation) {
            case N:
                if (!validatePosition(avatar.xCoordinate(), avatar.yCoordinate() - 1)) {
                    return false;
                }
                if (gameMechanism.squareBelongsToPlayer2(avatar.xCoordinate(), avatar.yCoordinate() - 1)) {
                    return false;
                }
                if (!gameMechanism.squareHasObstacle(avatar.xCoordinate(), avatar.yCoordinate() - 2)) {
                    return false;
                }
                if (!gameMechanism.squareHasObstacle(avatar.xCoordinate() - 1, avatar.yCoordinate() - 1)) {
                    return false;
                }
                if (!gameMechanism.squareHasObstacle(avatar.xCoordinate() + 1, avatar.yCoordinate() - 1)) {
                    return false;
                }
                break;
            case S:
                if (!validatePosition(avatar.xCoordinate(), avatar.yCoordinate() + 1)) {
                    return false;
                }
                if (gameMechanism.squareBelongsToPlayer2(avatar.xCoordinate(), avatar.yCoordinate() + 1)) {
                    return false;
                }
                if (!gameMechanism.squareHasObstacle(avatar.xCoordinate(), avatar.yCoordinate() + 2)) {
                    return false;
                }
                if (!gameMechanism.squareHasObstacle(avatar.xCoordinate() - 1, avatar.yCoordinate() + 1)) {
                    return false;
                }
                if (!gameMechanism.squareHasObstacle(avatar.xCoordinate() + 1, avatar.yCoordinate() + 1)) {
                    return false;
                }
                break;
            case E:
                if (!validatePosition(avatar.xCoordinate() + 1, avatar.yCoordinate())) {
                    return false;
                }
                if (gameMechanism.squareBelongsToPlayer2(avatar.xCoordinate() + 1, avatar.yCoordinate())) {
                    return false;
                }
                if (!gameMechanism.squareHasObstacle(avatar.xCoordinate() + 2, avatar.yCoordinate())) {
                    return false;
                }
                if (!gameMechanism.squareHasObstacle(avatar.xCoordinate() + 1, avatar.yCoordinate() + 1)) {
                    return false;
                }
                if (!gameMechanism.squareHasObstacle(avatar.xCoordinate() + 1, avatar.yCoordinate() - 1)) {
                    return false;
                }
                break;
            case W:
                if (!validatePosition(avatar.xCoordinate() - 1, avatar.yCoordinate())) {
                    return false;
                }
                if (gameMechanism.squareBelongsToPlayer2(avatar.xCoordinate() - 1, avatar.yCoordinate())) {
                    return false;
                }
                if (!gameMechanism.squareHasObstacle(avatar.xCoordinate() - 2, avatar.yCoordinate())) {
                    return false;
                }
                if (!gameMechanism.squareHasObstacle(avatar.xCoordinate() - 1, avatar.yCoordinate() + 1)) {
                    return false;
                }
                if (!gameMechanism.squareHasObstacle(avatar.xCoordinate() - 1, avatar.yCoordinate() - 1)) {
                    return false;
                }
                break;
            default:
                break;
        }
        return true;
    }

    private boolean scanForShotInADirection(Avatar avatar, Orientation.options orientation, GameMechanism gameMechanism) {
        int i = avatar.xCoordinate();
        int j = avatar.yCoordinate();

        switch (orientation) {
            case N:
                while (j >= 0 && !gameMechanism.squareHasObstacle(i, j)) {
                    if (gameMechanism.squareHasActiveShotFromPlayer2(i, j)) {
                        return true;
                    }
                    j--;
                }
                break;
            case S:
                while (j < maxLines && !gameMechanism.squareHasObstacle(i, j)) {
                    if (gameMechanism.squareHasActiveShotFromPlayer2(i, j)) {
                        return true;
                    }
                    j++;
                }
                break;
            case W:
                while (i >= 0 && !gameMechanism.squareHasObstacle(i, j)) {
                    if (gameMechanism.squareHasActiveShotFromPlayer2(i, j)) {
                        return true;
                    }
                    i--;
                }
                break;
            case E:
                while (i < maxColumns && !gameMechanism.squareHasObstacle(i, j)) {
                    if (gameMechanism.squareHasActiveShotFromPlayer2(i, j)) {
                        return true;
                    }
                    i++;
                }
                break;
            default:
                break;
        }
        return false;
    }

    private boolean validatePosition(int i, int j) {
        if (i < 0 || i > maxLines) {
            return false;
        }
        if (j < 0 || j > maxColumns) {
            return false;
        }
        return true;
    }
}
