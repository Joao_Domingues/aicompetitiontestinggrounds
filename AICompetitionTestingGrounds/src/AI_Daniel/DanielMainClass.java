package AI_Daniel;

import aicompetitiontestinggrounds.Avatar;
import aicompetitiontestinggrounds.GameMechanism;
import aicompetitiontestinggrounds.Orientation;
import java.awt.Point;
import java.util.Arrays;
import java.util.LinkedList;

public class DanielMainClass {

    int[] coordenadas = new int[]{8, 8};

    public DanielMainClass() {

    }

    private int[][] mapearBoard(Avatar avatar, GameMechanism gameMechanism) {
        int[][] boardCheat = new int[gameMechanism.boardNumberOfLines()][gameMechanism.boardNumberOfColumns()];

        for (int i = 0; i < gameMechanism.boardNumberOfLines(); i++) {
            for (int j = 0; j < gameMechanism.boardNumberOfColumns(); j++) {
                if (gameMechanism.squareHasObstacle(i, j) == true) {
                    boardCheat[i][j] = 1;
                } else {
                    boardCheat[i][j] = 0;
                }

            }

        }
        return boardCheat;
    }

    private void criarPath(Avatar avatar, GameMechanism gameMechanism) {
        int[][] boardCheat = mapearBoard(avatar, gameMechanism);
        Point start = new Point(8, 8);

        int[] x = {0, 0, 1, -1};//This represent 4 directions right, left, down , up
        int[] y = {1, -1, 0, 0};
        LinkedList<Point> q = new LinkedList();
        q.add(start);
        int n = boardCheat.length;
        int m = boardCheat[0].length;
        int[][] dist = new int[n][m];
        for (int[] a : dist) {
            Arrays.fill(a, -1);
        }
        dist[start.x][start.y] = 0;
        while (!q.isEmpty()) {
            Point p = q.removeFirst();
            for (int i = 0; i < 4; i++) {
                int a = p.x + x[i];
                int b = p.y + y[i];
                if (a >= 0 && b >= 0 && a < n && b < m && dist[a][b] == -1 && boardCheat[a][b] != 1) {
                    dist[a][b] = 1 + dist[p.x][p.y];
                    q.add(new Point(a, b));
                }
            }
        }
        while (true);
    }

    public void danielMove(Avatar avatar, GameMechanism gameMechanism) {

    }
}
