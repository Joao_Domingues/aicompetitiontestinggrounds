/*
* A collection of graph algorithms.
 */
package Utils;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author DEI-ESINF
 */
public class GraphAlgorithms {

    /**
     * Performs breadth-first search of a Graph starting in a Vertex
     *
     * @param g Graph instance
     * @param vInf information of the Vertex that will be the source of the
     * search
     * @return qbfs a queue with the vertices of breadth-first search
     */
    public static <V, E> LinkedList<V> BreadthFirstSearch(Graph<V, E> g, V vert) {

        if (!g.validVertex(vert)) {
            return null;
        }

        LinkedList<V> qbfs = new LinkedList<>();
        LinkedList<V> qaux = new LinkedList<>();
        boolean[] visited = new boolean[g.numVertices()];  //default initializ.: false

        qbfs.add(vert);
        qaux.add(vert);
        int vKey = g.getKey(vert);
        visited[vKey] = true;

        while (!qaux.isEmpty()) {
            vert = qaux.remove();
            for (Edge<V, E> edge : g.outgoingEdges(vert)) {
                V vAdj = g.opposite(vert, edge);
                vKey = g.getKey(vAdj);
                if (!visited[vKey]) {
                    qbfs.add(vAdj);
                    qaux.add(vAdj);
                    visited[vKey] = true;
                }
            }
        }
        return qbfs;
    }

    /**
     * Performs depth-first search starting in a Vertex
     *
     * @param g Graph instance
     * @param vOrig Vertex of graph g that will be the source of the search
     * @param visited set of discovered vertices
     * @param qdfs queue with vertices of depth-first search
     */
    private static <V, E> void DepthFirstSearch(Graph<V, E> g, V vOrig, boolean[] visited, LinkedList<V> qdfs) {
        qdfs.add(vOrig);
        visited[g.getKey(vOrig)] = true;
        for (V vAdj : g.adjVertices(vOrig)) {
            if (visited[g.getKey(vAdj)] == false) {
                DepthFirstSearch(g, vAdj, visited, qdfs);
            }
        }
    }

    /**
     * @param g Graph instance
     * @param vInf information of the Vertex that will be the source of the
     * search
     * @return qdfs a queue with the vertices of depth-first search
     */
    public static <V, E> LinkedList<V> DepthFirstSearch(Graph<V, E> g, V vert) {

        if (!g.validVertex(vert)) {
            return null;
        }

        LinkedList<V> resultQueue = new LinkedList<>();
        boolean[] knownVertices = new boolean[g.numVertices()];
        DepthFirstSearch(g, vert, knownVertices, resultQueue);
        return resultQueue;
    }

    /**
     * Returns all paths from vOrig to vDest
     *
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param vDest Vertex that will be the end of the path
     * @param visited set of discovered vertices
     * @param path stack with vertices of the current path (the path is in
     * reverse order)
     * @param paths ArrayList with all the paths (in correct order)
     */
    private static <V, E> void allPaths(Graph<V, E> g, V vOrig, V vDest, boolean[] visited,
            LinkedList<V> path, ArrayList<LinkedList<V>> paths) {

        int vAdjIdx;
        
        visited[g.getKey(vOrig)] = true;
        path.addFirst(vOrig);
        
        for(V vAdj : g.adjVertices(vOrig)) {
            if(vAdj.equals(vDest)) {
                path.addFirst(vDest);
                paths.add(revPath(path));
                path.removeFirst();
            } else if(visited[g.getKey(vAdj)] == false) {
                allPaths(g, vAdj, vDest, visited, path, paths);
            }
        }
        visited[g.getKey(vOrig)] = false;
        path.removeFirst();
    }

    /**
     * @param g Graph instance
     * @param voInf information of the Vertex origin
     * @param vdInf information of the Vertex destination
     * @return paths ArrayList with all paths from voInf to vdInf
     */
    public static <V, E> ArrayList<LinkedList<V>> allPaths(Graph<V, E> g, V vOrig, V vDest) {
        
        ArrayList<LinkedList<V>> paths = new ArrayList<>();

        if(g.numVertices() == 0 || g.validVertex(vOrig) == false || g.validVertex(vDest) == false) return null;
        
        boolean[] knownVertices = new boolean[g.numVertices()];
        
        LinkedList<V> auxStack = new LinkedList<>();
        
        allPaths(g, vOrig, vDest, knownVertices, auxStack, paths);
        
        return paths;
    }

    /**
     * Computes shortest-path distance from a source vertex to all reachable
     * vertices of a graph g with nonnegative edge weights This implementation
     * uses Dijkstra's algorithm
     *
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param visited set of discovered vertices
     * @param pathkeys minimum path vertices keys
     * @param dist minimum distances
     */
    private static <V, E> void shortestPathLength(Graph<V, E> g, V vOrig, V[] vertices,
            boolean[] visited, int[] pathKeys, double[] dist) {

        int vKey = g.getKey(vOrig);
        dist[vKey] = 0;
        
        while(vKey != -1) {
            vOrig = vertices[vKey];
            visited[vKey] = true;
            for(V vAdj : g.adjVertices(vOrig)) {
                int vkeyAdj = g.getKey(vAdj);
                Edge<V,E> edge = g.getEdge(vOrig, vAdj);
                if (!visited[vkeyAdj] && dist[vkeyAdj] > dist[vKey]+edge.getWeight()) {
                    dist[vkeyAdj] = dist[vKey] + edge.getWeight();
                    pathKeys[vkeyAdj]=vKey;
                }
            }
            double minDist = Double.MAX_VALUE;
            vKey = -1;
            for(int i = 0; i < g.numVertices(); i++) {
                if(!visited[i] && dist[i] < minDist) {
                    minDist = dist[i];
                    vKey = i;
                }
            }
        }
    }

    /**
     * Extracts from pathKeys the minimum path between voInf and vdInf The path
     * is constructed from the end to the beginning
     *
     * @param g Graph instance
     * @param voInf information of the Vertex origin
     * @param vdInf information of the Vertex destination
     * @param pathkeys minimum path vertices keys
     * @param path stack with the minimum path (correct order)
     */
    private static <V, E> void getPath(Graph<V, E> g, V vOrig, V vDest, V[] verts, int[] pathKeys, LinkedList<V> path) {

        if(!vOrig.equals(vDest)) {
            path.push(vDest);
            int vDestIdx = g.getKey(vDest);
            int DestIdx = pathKeys[vDestIdx];
            vDest = verts[DestIdx];
            getPath(g, vOrig, vDest, verts, pathKeys, path);
        } else {
            path.push(vOrig);
        }
    }

    //shortest-path between voInf and vdInf
    public static <V, E> double shortestPath(Graph<V, E> g, V vOrig, V vDest, LinkedList<V> shortPath) {

        if(!g.validVertex(vOrig) || !g.validVertex(vDest)) return 0;

        V vertices[] = g.allkeyVerts();
        boolean visited[] = new boolean[g.numVertices()];
        int pathKeys[] = new int[g.numVertices()];
        double dist[] = new double[g.numVertices()];
        
        for(int i = 0; i < g.numVertices(); i++) {
            dist[i] = Double.MAX_VALUE;
            pathKeys[i] = -1;
        }
        
        shortestPathLength(g, vOrig, vertices, visited, pathKeys, dist);
        
        double lengthPath = dist[g.getKey(vDest)];
        
        if(lengthPath != Double.MAX_VALUE) {
            getPath(g,vOrig,vDest,vertices,pathKeys,shortPath);
            return lengthPath;
        }
        
        return 0;
    }

    /**
     * Reverses the path
     *
     * @param path stack with path
     */
    private static <V, E> LinkedList<V> revPath(LinkedList<V> path) {

        LinkedList<V> pathcopy = new LinkedList<>(path);
        LinkedList<V> pathrev = new LinkedList<>();

        while (!pathcopy.isEmpty()) {
            pathrev.push(pathcopy.pop());
        }

        return pathrev;
    }
}
