package aicompetitiontestinggrounds;

import java.util.ArrayList;
import java.util.List;

public class Square {

    protected int line;
    protected int column;

    private boolean obstacle = false;
    protected List<Avatar> avList = new ArrayList<>();
    protected List<ActiveShot> asList = new ArrayList<>();

    private int playerID = 0;

    protected Square(int line, int column) {
        this.line = line;
        this.column = column;
    }

    protected void insertObstacle() {
        obstacle = true;
    }

    protected boolean checkCoordinates(int i, int j) {
        return line == i && column == j;
    }

    /**
     * Adds an avatar, checking if it can first
     *
     * @param avatar
     * @return
     */
    protected boolean addAvatar(Avatar avatar) {
        if (hasObstacle()) {
            return false;
        }
        if (!avList.contains(avatar)) {
            avList.add(avatar);
        }
        return true;
    }

    protected void removeAvatar(Avatar avatar) {
        avList.remove(avatar);
    }

    protected boolean addActiveShot(ActiveShot as) {
        if (hasObstacle()) {
            return false;
        }
        asList.add(as);
        return true;
    }

    protected void clearActiveShots() {
        asList.clear();
    }

    protected void clearAvatars() {
        avList.clear();
    }

    protected void paintPlayerID(int playerID) {
        this.playerID = playerID;
    }

    /**
     * Looks redundant, but it is the correct way to do it in this situation
     * Accessible by the player
     *
     * @return
     */
    public boolean hasObstacle() {
        if (obstacle) {
            return true;
        }
        return false;
    }

    protected boolean hasPlayer() {
        return avList.size() > 0;
    }

    protected boolean hasActiveShot() {
        return asList.size() > 0;
    }

    protected boolean hasMultipleActiveShots() {
        return asList.size() > 1;
    }

    protected List<ActiveShot> listOfActiveShots() {
        return asList;
    }

    protected void paint() {
        if (!asList.isEmpty()) {
            playerID = asList.get(0).playerID();
            return;
        }
        if (avList.size() == 1) {
            playerID = avList.get(0).playerID();
        }
    }

    protected boolean belongsToPlayer1() {
        return playerID == 1;
    }

    protected boolean belongsToPlayer2() {
        return playerID == 2;
    }
    
    protected void info() {
        System.out.println("\nSquare: " + line + "/" + column +
                "\nPlayerID - " + playerID +
                "\nObstacle - " + hasObstacle());
        if(!avList.isEmpty()) {
            for(Avatar av : avList) {
                System.out.println("Avatar - " + av.playerID());
            }
        }
        if(!asList.isEmpty()) {
            for(ActiveShot av : asList) {
                System.out.println("ActiveShot - " + av.playerID());
            }
        }
    }
    
    protected boolean hasMultipleAvatars() {
        return avList.size() > 1;
    }
}
