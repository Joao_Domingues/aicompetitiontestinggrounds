package aicompetitiontestinggrounds;

public class ActiveShot {

    private int playerID;

    private int line;
    private int column;

    protected Orientation.options orientation;

    protected ActiveShot(int playerID, int line, int column, Orientation.options orientation) {
        this.playerID = playerID;
        this.line = line;
        this.column = column;
        this.orientation = orientation;
    }

    protected void move() {
        switch (orientation) {
            case N:
                column--;
                break;
            case S:
                column++;
                break;
            case E:
                line++;
                break;
            case W:
                line--;
                break;
            default:
                break;
        }
    }

    protected int xCoordinate() {
        int x = line;
        return x;
    }

    protected int yCoordinate() {
        int y = column;
        return y;
    }

    protected int playerID() {
        int playerID = this.playerID;
        return playerID;
    }
}
