package aicompetitiontestinggrounds;

import java.util.ArrayList;
import java.util.List;

public class Board {

    private int lines;
    private int columns;

    protected List<Square> squares = new ArrayList<>();
    private List<Avatar> avatars = new ArrayList<>();
    private List<ActiveShot> activeShots = new ArrayList<>();

    protected Board(int lines, int columns, Avatar avatar1, Avatar avatar2) {
        this.lines = lines;
        this.columns = columns;
        avatars.add(avatar1);
        avatars.add(avatar2);
        setUpBoard();
        getSquareAtPosition(avatar1.xCoordinate(), avatar1.yCoordinate()).addAvatar(avatar1);
        getSquareAtPosition(avatar2.xCoordinate(), avatar2.yCoordinate()).addAvatar(avatar2);
    }

    private void setUpBoard() {
        for (int i = 0; i < lines; i++) {
            for (int j = 0; j < columns; j++) {
                squares.add(new Square(i, j));
            }
        }
    }

    protected void insertObstacleInPositionXY(int i, int j) {
        for (Square square : squares) {
            if (square.checkCoordinates(i, j)) {
                square.insertObstacle();
            }
        }
    }

    protected Square getSquareAtPosition(int i, int j) {
        for (Square square : squares) {
            if (square.checkCoordinates(i, j)) {
                return square;
            }
        }
        return null;
    }

    protected boolean createShot(Avatar avatar, Orientation.options orientation) {
        if(getSquareAtPosition(avatar.xCoordinate(), avatar.yCoordinate()).hasMultipleAvatars()) return false;
        
        if (!validatePositionForNewActiveShot(avatar.xCoordinate(), avatar.yCoordinate(), orientation)) {
            return false;
        }

        ActiveShot as = avatar.shoot(orientation);
        activeShots.add(as);
        
        return true;
    }

    protected void moveAllShots() {
        for (Square square : squares) {
            square.clearActiveShots();
        }

        List<ActiveShot> auxList = new ArrayList<>();

        // moves all shots, removes the ones out of boundaries
        for (ActiveShot as : activeShots) {
            as.move();
            if (!validatePositionForExistentActiveShot(as.xCoordinate(), as.yCoordinate())) {
                auxList.add(as);
            } else {
                getSquareAtPosition(as.xCoordinate(), as.yCoordinate()).addActiveShot(as);
            }
        }
        
        activeShots.removeAll(auxList);
        auxList.clear();

        for (Square square : squares) {
            if (square.hasMultipleActiveShots() || square.hasObstacle() || square.hasPlayer()) {
                auxList.addAll(square.listOfActiveShots());
                square.clearActiveShots();
            }
        }

        activeShots.removeAll(auxList);

    }

    protected void moveAllAvatars() {
        for (Square square : squares) {
            square.clearAvatars();
        }

        for (Avatar av : avatars) {
            getSquareAtPosition(av.xCoordinate(), av.yCoordinate()).addAvatar(av);
        }
    }

    private boolean validatePositionForNewActiveShot(int line, int column, Orientation.options orientation) {
        switch (orientation) {
            case N:
                column--;
                break;
            case S:
                column++;
                break;
            case E:
                line++;
                break;
            case W:
                line--;
                break;
            default:
                break;
        }

        if (getSquareAtPosition(line, column) == null) {
            return false;
        }
        if (getSquareAtPosition(line, column).hasObstacle()) {
            return false;
        }
        if (getSquareAtPosition(line, column).hasPlayer()) {
            return false;
        }
        if (getSquareAtPosition(line, column).hasActiveShot()) {
            return false;
        }

        switch (orientation) {
            case N:
                if (line == 0) {
                    return false;
                }
            case S:
                if (line == lines - 1) {
                    return false;
                }
            case E:
                if (column == columns - 1) {
                    return false;
                }
            case W:
                if (column == 0) {
                    return false;
                }
            default:
                break;
        }

        return true;
    }

    protected boolean validatePositionForAvatar(int i, int j) {
        if (!validatePosition(i, j)) {
            return false;
        }
        if (getSquareAtPosition(i, j).hasObstacle()) {
            return false;
        }
        return true;
    }
    
    protected boolean validatePositionForExistentActiveShot(int i, int j) {
        if (!validatePosition(i, j)) {
            return false;
        }
        if (getSquareAtPosition(i, j).hasObstacle() || getSquareAtPosition(i, j).hasPlayer()) {
            return false;
        }
        return true;
    }

    private boolean validatePosition(int i, int j) {
        if (i < 0 || i >= lines) {
            return false;
        }
        if (j < 0 || j >= columns) {
            return false;
        }
        return true;
    }

    protected void repaintBoard() {
        for (Square square : squares) {
            square.paint();
        }
    }

    protected Avatar getAvatar(int i) {
        return avatars.get(i);
    }
    
    protected int getLines() {
        int x = lines;
        return x;
    }
    
    protected int getColumns() {
        int x = columns;
        return x;
    }

    protected void deleteActionShots() {
        List<ActiveShot> auxList = new ArrayList<>();
        for(Square square : squares) {
            if(square.hasMultipleAvatars()) {
                for(Avatar av : square.avList) {
                    for(ActiveShot as : activeShots) {
                        if(as.playerID() == av.playerID()) auxList.add(as);
                    }
                }
            }
        }
        activeShots.remove(auxList);
    }

    protected String scoreBoard() {
        int x = 0, y = 0;
        for(Square square : squares) {
            if(square.belongsToPlayer1()) x++;
            if(square.belongsToPlayer2()) y++;
        }
        return "Player1: " + x + "\nPlayer2: " + y;
    }
}
