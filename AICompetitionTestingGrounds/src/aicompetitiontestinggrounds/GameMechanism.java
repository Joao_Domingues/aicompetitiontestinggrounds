package aicompetitiontestinggrounds;

import AI_Daniel.DanielMainClass;
import AI_Joao.JoaoMainClass;
import AI_Joao_2.JoaoMainClass2;
import java.util.List;

public class GameMechanism {

    private Board board;
    private JoaoMainClass player1 = new JoaoMainClass();
    private JoaoMainClass2 player2 = new JoaoMainClass2();
//    private DanielMainClass player2 = new DanielMainClass();

    private int maxTurns;
    private int turn = 0;

    protected GameMechanism(int i, int j, int nTurns, List<int[]> ints, int x1, int y1, int x2, int y2) {
        board = new Board(i, j, new Avatar(1, x1, y1), new Avatar(2, x2, y2));
        for (int[] pair : ints) {
            board.insertObstacleInPositionXY(pair[0], pair[1]);
        }
        maxTurns = nTurns;
    }

    protected void advanceOneTurn() {
        System.out.println("\n--- Turn " + turn + " ---");
        turn++;
        player1.joaoMove(board.getAvatar(0), this);
        player2.joaoMove(board.getAvatar(1), this);
//        player2.danielMove(board.getAvatar(1), this);

        board.moveAllAvatars();
        board.deleteActionShots();
        board.moveAllShots();
        board.repaintBoard();
        System.out.println(board.scoreBoard());
    }
    
    public boolean playerMoveAction(Avatar avatar, Orientation.options orientation) {
        int i, j;
        switch (orientation) {
            case N:
                i = avatar.xCoordinate();
                j = avatar.yCoordinate()-1;
                break;
            case S:
                i = avatar.xCoordinate();
                j = avatar.yCoordinate()+1;
                break;
            case E:
                i = avatar.xCoordinate()+1;
                j = avatar.yCoordinate();
                break;
            case W:
                i = avatar.xCoordinate()-1;
                j = avatar.yCoordinate();
                break;
            default:
                i = avatar.xCoordinate();
                j = avatar.yCoordinate();
                break;
        }
        if(!board.validatePositionForAvatar(i, j)) return false;
        avatar.moveAvatarToPositionXY(i, j);
        System.out.println("MoveAction - Avatar: " + avatar.playerID() + "\n x: " + avatar.xCoordinate() + "| y: " + avatar.yCoordinate() + " \n Orientation: " + orientation);
        return true;
    }

    public boolean playerShootAction(Avatar avatar, Orientation.options orientation) {
        if (!board.createShot(avatar, orientation)) {
            return false;
        }
        System.out.println("ShootAction - Avatar: " + avatar.playerID() + " \n Orientation: " + orientation);
        return true;
    }
    
    public boolean squareHasPlayer(int i, int j) {
        if(board.getSquareAtPosition(i, j) == null) return false;
        return board.getSquareAtPosition(i, j).hasPlayer();
    }
    
    public boolean squareHasObstacle(int i, int j) {
        if(board.getSquareAtPosition(i, j) == null) return true;
        return board.getSquareAtPosition(i, j).hasObstacle();
    }
    
    public boolean squareHasActiveShotFromPlayer1(int i, int j) {
        if(board.getSquareAtPosition(i, j) == null) return false;
        if(!board.getSquareAtPosition(i, j).hasActiveShot()) return false;
        return board.getSquareAtPosition(i, j).asList.get(0).playerID() == 1;
    }
    
    public boolean squareHasActiveShotFromPlayer2(int i, int j) {
        if(board.getSquareAtPosition(i, j) == null) return false;
        if(!board.getSquareAtPosition(i, j).hasActiveShot()) return false;
        return board.getSquareAtPosition(i, j).asList.get(0).playerID() == 2;
    }
    
    public boolean squareBelongsToPlayer1(int i, int j) {
        if(board.getSquareAtPosition(i, j) == null) return false;
        return board.getSquareAtPosition(i, j).belongsToPlayer1();
    }
    
    public boolean squareBelongsToPlayer2(int i, int j) {
        if(board.getSquareAtPosition(i, j) == null) return false;
        return board.getSquareAtPosition(i, j).belongsToPlayer2();
    }
    
    public int boardNumberOfLines() {
        return board.getLines();
    }
    
    public int boardNumberOfColumns() {
        return board.getColumns();
    }
    
    public Orientation.options orientationOfActiveShot(int i, int j) {
        if(board.getSquareAtPosition(i, j) == null) return null;
        if(!board.getSquareAtPosition(i, j).hasActiveShot()) return null;
        return board.getSquareAtPosition(i, j).asList.get(0).orientation;
    }

    protected void addObstacle(int i, int j) {
        board.insertObstacleInPositionXY(i, j);
    }

    protected void moveAvatar1SetUp(int i, int j) {
        board.getAvatar(0).moveAvatarToPositionXY(i, j);
    }

    protected void moveAvatar2SetUp(int i, int j) {
        board.getAvatar(1).moveAvatarToPositionXY(i, j);
    }

    protected Board getBoard() {
        return this.board;
    }
}
