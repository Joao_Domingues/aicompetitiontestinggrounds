package aicompetitiontestinggrounds;

public class Avatar {

    private int playerID;

    private int line;
    private int column;

    protected Avatar(int playerID, int x, int y) {
        this.playerID = playerID;
        this.line = x;
        this.column = y;
    }

    protected void moveAvatarToPositionXY(int i, int j) {
        line = i;
        column = j;
    }

    protected ActiveShot shoot(Orientation.options orientation) {
        return new ActiveShot(playerID, line, column, orientation);
    }

    public int xCoordinate() {
        int x = line;
        return x;
    }

    public int yCoordinate() {
        int y = column;
        return y;
    }

    protected int playerID() {
        int playerID = this.playerID;
        return playerID;
    }
}
