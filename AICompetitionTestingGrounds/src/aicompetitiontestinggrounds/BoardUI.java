package aicompetitiontestinggrounds;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class BoardUI extends JPanel {

    private GameMechanism gm;
    private int lines;
    private int columns;

    public BoardUI(GameMechanism gm) {
        this.gm = gm;
        lines = gm.getBoard().getLines() + 1;
        columns = gm.getBoard().getColumns() + 1;
    }

    ;
    
    @Override
    public void paint(Graphics g) {
        g.setColor(Color.BLACK);
        for (int i = 50; i <= lines * 25; i += 25) {
            for (int j = 50; j <= columns * 25; j += 25) {
                g.drawRect(i, j, 25, 25);
            }
        }

        for (Square square : gm.getBoard().squares) {
            int line = 50 + 25 * square.line;
            int column = 50 + 25 * square.column;
            if (square.hasObstacle()) {
                g.setColor(Color.BLACK);
                g.fillRect(line, column, 25, 25);
            } else if (square.hasPlayer()) {
                for(Avatar av : square.avList) {
                    if(av.playerID() == 1) {
                        g.setColor(Color.RED);
                    } else {
                        g.setColor(Color.BLUE);
                    }
                g.drawOval(line, column, 25, 25);
                }
            } else if (square.hasActiveShot()) {
                g.setColor(Color.BLACK);
                g.drawOval(line + 6, column + 6, 13, 13);
            } else if (square.belongsToPlayer1()) {
                g.setColor(Color.RED);
                g.fillRect(line, column, 25, 25);
            } else if (square.belongsToPlayer2()) {
                g.setColor(Color.BLUE);
                g.fillRect(line, column, 25, 25);
            }
        }

    }
    
}
